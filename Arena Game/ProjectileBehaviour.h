#ifndef PROJECTILEBEHAVIOUR_H_
#define PROJECTILEBEHAVIOUR_H_

class ProjectileBehaviour
{
public:
	ProjectileBehaviour(void);
	~ProjectileBehaviour(void);

	void simpleShot();
	void spreadShot();
};
#endif 

