#ifndef MELEE_H_
#define MELEE_H_

#include "entity.h"
#include "IUpdatable.h"
#include "PausableClock.h"

class Melee : public Entity, public IUpdatable
{
public:
	Melee();
	~Melee(void);

	virtual void Update();
	void setOrigin(const sf::Vector2f origin){ this-> origin = origin; }
	void setOffSet(const sf::Vector2f offset){ this->offset = offset; }
	void setSize(const sf::Vector2f size){ this->size = size; }
	void setDuration(const sf::Time duration){ this->duration = duration; }
	void setCollisionBox();

private:
	sf::Vector2f origin;
	sf::Vector2f offset;
	sf::Vector2f size;
	sf::Time duration;
	PausableClock pClock;
};
#endif
