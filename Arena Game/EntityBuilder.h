#ifndef ENTITYBUILDER_H_
#define ENTITYBUILDER_H_

#include <string>
#include <SFML\Graphics.hpp>

#include "EntityTypes.h"
#include "ContentManager.h"
#include "EntityManager.h"
#include "Player.h"
#include "Enemy.h"
#include "Projectile.h"
#include "ExplosiveTrap.h"
#include "Melee.h"

/*
	Make sure all entities are taking in an ID, melee isn't yet
*/

class EntityBuilder
{
public:
	static void init(ContentManager *contentManagerPtr, EntityManager *entityManagerPtr);
	static void release();

	static void buildEntity(Entity *entityPtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f speed = sf::Vector2f(0, 0));
	static void buildProjectile(Projectile *ProjectilePtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f direction, const sf::Vector2f speed,const int playerID);
	static void buildEnemy(Enemy *enemyPtr, const std::string textureFilePath, const Entity &target, AI *behaviourPtr, const sf::Vector2f position, const sf::Vector2f speed);
	static void buildPlayer(Player *playerPtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f speed, unsigned int id);
	static void buildMelee(Melee *meleePtr, const sf::Vector2f origin, const sf::Vector2f offset, const sf::Vector2f size, const sf::Time duration);

private:
	static ContentManager *contentManagerPtr;
	static EntityManager *entityManagerPtr;
};

#endif

