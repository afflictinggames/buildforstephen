#include "PauseMenu.h"

PauseMenu::PauseMenu()
{

}

PauseMenu::~PauseMenu()
{

}

void PauseMenu::initMap(sf::RenderWindow &window, ContentManager &content)
{
	//hardcoded positions
	sf::RectangleShape resumeButton;
	resumeButton.setSize(sf::Vector2f(400.0f, 200.0f));
	resumeButton.setTexture(content.getTexture("Content/Images/playButton.png"));
	resumeButton.setPosition(window.getSize().x / 2 - resumeButton.getSize().x / 2, 200);
	menuItems[RESUME] = resumeButton;

	sf::RectangleShape restartButton;
	restartButton.setSize(sf::Vector2f(400.0f, 200.0f));
	restartButton.setTexture(content.getTexture("Content/Images/playButton.png"));
	restartButton.setPosition(window.getSize().x / 2 - restartButton.getSize().x / 2, 400);
	menuItems[RESTART] = restartButton;

}
