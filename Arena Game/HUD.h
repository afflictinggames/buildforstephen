#ifndef HUD_H_
#define HUD_H_

#include "Global.h"
#include "Player.h"
#include <sstream>

class HUD
{
public:
	HUD();
	~HUD();

	void init(std::vector<Entity*> *players);
	void Draw(sf::RenderWindow& window);
	void Update();
	void Flush();

private:
	std::vector<Player*> *players;
	std::vector<sf::RectangleShape> healthRecs;
	sf::Font scoreFont;
	sf::Text scoreText;
	std::stringstream sstream;
};

#endif