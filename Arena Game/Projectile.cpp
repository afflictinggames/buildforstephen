#include "Projectile.h"

Projectile::Projectile()
{
	type   = PROJECTILE ;
	damage = 10.f       ;
}

Projectile::~Projectile(void)
{
}

void Projectile::Update()
{
	move(velocity);

	if(colliding)
		setActive(false);
}

void Projectile::move(sf::Vector2f &movement)
{
	setPos(getPos() + movement);
}

void Projectile::setVelocity(const sf::Vector2f &direction, const sf::Vector2f &speed)
{
	velocity = VectorFuncs::normalise(direction);
	velocity = VectorFuncs::mult(velocity,speed);
}

const sf::Vector2f *Projectile::getVelocity() const
{
	return &velocity;
}
