#include "HUD.h"

HUD::HUD()
{

}

HUD::~HUD()
{
	Flush();
	delete this->players;
}

void HUD::init(std::vector<Entity*> *players)
{
	scoreFont.loadFromFile("Content/Fonts/calibri.ttf");
	scoreText.setFont(scoreFont);
	scoreText.setPosition(200,200);
	scoreText.setColor(sf::Color::Black);

	this->players = new std::vector<Player*>();

	for(unsigned int i = 0; i < players->size(); i++)
	{
		Player *playerPtr = dynamic_cast<Player*>(players->at(i)) ;

		if(playerPtr != NULL)
		{
			this->players->push_back(playerPtr);

			healthRecs.push_back(sf::RectangleShape(this->players->at(i)->getPlayerBars().health.getSize()));
			healthRecs.at(i).setTexture(this->players->at(i)->getPlayerBars().health.getTexture());
			healthRecs.at(i).setTextureRect(this->players->at(i)->getPlayerBars().health.getTextureRect());
		}
	}
}

void HUD::Draw(sf::RenderWindow& window)
{
	for(unsigned int i = 0; i < healthRecs.size(); i++)
	{
		window.draw(healthRecs.at(i));
	}
	window.draw(scoreText);
}

void HUD::Update()
{
	for(unsigned int i = 0; i < healthRecs.size(); i++)
	{
		//healthRecs.at(i).setTextureRect(sf::IntRect(0,0,(players->at(i)->getHealth() / players->at(i)->getMaxHealth()) * players->at(i)->getPlayerBars().health.getTexture()->getSize().x, players->at(i)->getPlayerBars().health.getTexture()->getSize().y));
		//healthRecs.at(i).setScale( (players->at(i)->getHealth() / players->at(i)->getMaxHealth()), 1.0f);

		sstream.str("");
		sstream << "Score: " << players->at(0)->getScore();
		scoreText.setString(sstream.str());
	}
}

void HUD::Flush()
{
	players->clear();
	healthRecs.clear();
}