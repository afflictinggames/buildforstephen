#ifndef SPRITESHEET_H_
#define SPRITESHEET_H_

#include <SFML\Graphics.hpp>

struct SpriteSheet
{
	SpriteSheet()
	{

	}

	void load(sf::Texture *texturePtr, sf::Vector2u numFrames)
	{
		this->texturePtr = texturePtr;
		this->numFrames = numFrames;
		this->frameSize = sf::Vector2u(texturePtr->getSize().x / numFrames.x, texturePtr->getSize().y / numFrames.y);
	}

	sf::Texture *texturePtr;
	sf::Vector2u numFrames;
	sf::Vector2u frameSize;

};

#endif