#ifndef EXPLOSIVETRAP_H_
#define EXPLOSIVETRAP_H_

#include "Trap.h"
#include "Projectile.h"
#include "ContentManager.h"
#include "EntityManager.h"

class ExplosiveTrap : public Trap
{
public:
	ExplosiveTrap();
	~ExplosiveTrap();

	virtual void initAnimation();

protected:

};

#endif
