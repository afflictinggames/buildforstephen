#ifndef CONTENTMANAGER_H_
#define CONTENTMANAGER_H_

#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include <string>
#include <iostream>

class ContentManager
{
public:
	ContentManager();
	~ContentManager();

	//adds a texture to the manager and loads it from memory
	void addTexture(std::string filepath);

	//removes a texture from the manager
	void removeTexture(std::string filepath);

	//Gets a texture via the filepath
	const sf::Texture *getTexture(std::string filepath);

	void flush();

private:
	//Holds all the texture pointers. Can be accessed via filepath
	std::map<std::string, sf::Texture*> textures;

	//Iterator to iterate through the texture pointer list
	std::map<std::string, sf::Texture*>::const_iterator it;

};

#endif

