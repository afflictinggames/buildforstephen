#ifndef ENEMY_H_
#define ENEMY_H_

#include "Character.h"
#include "SimpleAI.h"
#include "BasicAI.h"
#include "AdvancedAI.h"
#include "FleeingAI.h"
#include "DefensiveAI.h"

class Enemy : public Character
{
public:
	Enemy();
	~Enemy();

	virtual void Update();
	virtual void move(const sf::Vector2f &movement);
	virtual void initAnimation();
	virtual void initAbilities();
	
	void setBehaviour(AI *behaviourPtr);
	void setTarget(const Entity *targetPtr);

	const Entity *getTarget() const { return targetPtr; }
	const AI *getBehaviour() const { return behaviourPtr; }

	void setProjectileList(const std::vector<Entity *> *listPtr);
	const std::vector<Entity *> *getProjectileList() const;

private:
	const Entity *targetPtr;
	AI *behaviourPtr;

	const std::vector<Entity *> *projectileList;
};

#endif
