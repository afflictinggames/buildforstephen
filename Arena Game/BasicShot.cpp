#include "BasicShot.h"
#include "EntityBuilder.h"

BasicShot::BasicShot(sf::Time rateOfFireLama)
	: Ability(rateOfFireLama)
{

}

BasicShot::~BasicShot()
{

}

void BasicShot::applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction,const int playerID)
{
	EntityBuilder::buildProjectile(new Projectile(), "Content/Images/bullet.png", position, direction, sf::Vector2f(5, 5),playerID);
	canFire = false;
}
