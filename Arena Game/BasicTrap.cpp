#include "BasicTrap.h"
#include "EntityBuilder.h"

BasicTrap::BasicTrap(sf::Time rateOfFireLama)
	: Ability(rateOfFireLama)
{

}

void BasicTrap::applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction, const int playerID)
{
	EntityBuilder::buildEntity(new Trap(), "Content/Images/bullet.png", position);
	canFire = false;
}