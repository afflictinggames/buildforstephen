#ifndef PROJECTILE_H_
#define PROJECTILE_H_

#include "entity.h"
#include "IUpdatable.h"

class Projectile : public Entity, public IUpdatable
{
public:
	Projectile();
	~Projectile();

	virtual void Update();
	virtual void move(sf::Vector2f &movement);

	void setVelocity (const sf::Vector2f &direction, const sf::Vector2f &speed);
	void setDamage   (float damage) { this -> damage   = damage   ;}
	void setPlayerID (int playerID) { this -> playerID = playerID ;}

	const sf::Vector2f *getVelocity() const;
	const float getDamage          () {return damage   ;}
	const int   getPlayerID        () {return playerID ;}
	

private:
	sf::Vector2f speed      ;
	sf::Vector2f direction  ;
	sf::Vector2f velocity   ;
	float        damage     ;
	int          playerID   ;
};

#endif

