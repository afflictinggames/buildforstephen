#include "BasicMelee.h"
#include "EntityBuilder.h"

BasicMelee::BasicMelee(sf::Time rateOfFireLama)
	: Ability(rateOfFireLama)
{

}

BasicMelee::~BasicMelee()
{

}

void BasicMelee::applyAbility(const sf::Vector2f& position, const sf::Vector2f& direction, const int playerID)
{
	EntityBuilder::buildMelee(new Melee(), position, sf::Vector2f(10,10) , sf::Vector2f(20,20), sf::seconds(2));
	canFire = false;
}