#include "Entity.h"

Entity::Entity()
	: active(true)
{
	colliding = false;
	
}

Entity::~Entity()
{

}

void Entity::Load(const sf::Texture &texture)
{
	sprite.setTexture(texture);
	collisionBox = sf::IntRect((int)getPos().x, (int)getPos().y, sprite.getTextureRect().width, sprite.getTextureRect().height);
}


void Entity::Draw(sf::RenderWindow& window)
{
	window.draw(sprite);
}

void Entity::setFrame(const sf::IntRect &rect)
{
	//set the texture rectangle in the sprite sheet
	sprite.setTextureRect(rect);
	collisionBox = sf::IntRect((int)getPos().x, (int)getPos().y, sprite.getTextureRect().width, sprite.getTextureRect().height);
}

void Entity::setPos(const sf::Vector2f& position)
{
	sprite.setPosition(position);
	collisionBox = sf::IntRect((int)position.x, (int)position.y, sprite.getTextureRect().width, sprite.getTextureRect().height);
}





