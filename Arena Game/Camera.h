#ifndef CAMERA_H_
#define CAMERA_H_

#include "Global.h"
#include "Player.h"
#include "InputHandler.h"

class Camera
{
public:
	Camera  (sf::RenderWindow& window) ;
	~Camera ()                         ;

	void Update    (Player& player,sf::RenderWindow& window)  ;

	void moveCamera(Player& player,sf::RenderWindow& window) ;


private:
	/*
	*View that will always change
	*/
	sf::View standard;

};
#endif
