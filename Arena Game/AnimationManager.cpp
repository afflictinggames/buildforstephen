#include "AnimationManager.h"

AnimationManager::AnimationManager()
	: onLoop(false), currentFrameIndex(0)
{
	
}

AnimationManager::~AnimationManager()
{
	Flush();
}

void AnimationManager::addAnimation(const std::string &animationName, Animation *animation)
{
	if(animation != NULL)
		animationList[animationName] = animation;
}

void AnimationManager::removeAnimation(const std::string &animationName)
{
	//only remove the animation if we find it in the list
	if(animationList.find(animationName) != animationList.end())
	{
		animationList.erase(animationName);
		delete animationList[animationName];
	}
}

void AnimationManager::playAnimation(Entity &targetPtr, const std::string &animationName, const bool loop)
{
	//only play the animation if it exists in the map
	if(animationList.find(animationName) != animationList.end())
	{
		//sometimes, they might want to play the same animation but change the loop state
		if(onLoop != loop)
			onLoop = loop;

		//if it's not already playing
		if(currentAnimationPtr != animationList[animationName])
		{
			onLoop = loop;
			currentAnimationPtr = animationList[animationName];
			resetCurrentAnimation(targetPtr);
			pClock.restart();
		}
	}
}

void AnimationManager::setDefaultAnimation(Entity &targetPtr, const std::string &animationName, const bool loop)
{
	onLoop = loop;
	currentAnimationPtr = animationList[animationName];
	resetCurrentAnimation(targetPtr);
}

void AnimationManager::pauseCurrentAnimation()
{
	pClock.pause();
}

void AnimationManager::resumeCurrentAnimation()
{
	pClock.resume();
}

void AnimationManager::stopCurrentAnimation()
{
	//will we need a default animation variable?
}

void AnimationManager::Update(Entity &targetPtr)
{
	//if it's time to move onto the next frame
	//check if we still need to animate or stop
	if(!onLastFrame() || onLoop)
	{
		if(pClock.getElapsedTime() >= currentFramePtr->duration)
		{
			advanceFrame();
			targetPtr.setFrame(currentFramePtr->rect);
			pClock.restart();
		}
	}
}

void AnimationManager::resetCurrentAnimation(Entity &targetPtr)
{
	currentFrameIndex = 0;
	currentFramePtr = currentAnimationPtr->getFrameAt(currentFrameIndex);
	targetPtr.setFrame(currentFramePtr->rect);
}

void AnimationManager::advanceFrame()
{
	if(onLastFrame())
		currentFrameIndex = 0;
	else
		currentFrameIndex++;

	currentFramePtr = currentAnimationPtr->getFrameAt(currentFrameIndex);
}

bool AnimationManager::onLastFrame()
{
	return currentFrameIndex == currentAnimationPtr->getNumFrames() - 1;
}

void AnimationManager::Flush()
{
	for(it = animationList.begin(); it != animationList.end(); it++)
	{
		delete it->second;
	}
	animationList.clear();
}