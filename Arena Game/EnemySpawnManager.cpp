#include "EnemySpawnManager.h"


EnemySpawnManager::EnemySpawnManager(sf::Time spawnRate)
	: spawnRate(spawnRate)
{
	srand((unsigned int)time(NULL));
}


EnemySpawnManager::~EnemySpawnManager(void)
{

}

void EnemySpawnManager::addSpawnPoint(SpawnPoint spawnPoint)
{
	spawnPoints.push_back(spawnPoint);
}

void EnemySpawnManager::Update(const std::vector<Entity*> &targets)
{
	
	if(spawnClock.getElapsedTime() >= spawnRate)
	{
		//pick random target for enemy
		targetPtr = (targets.at(rand() % targets.size()));
		if(targetPtr == NULL)
			return;

		//pick random spawn
		//NOTE: If size is 0, it will cause an error is N mod 0 is undefined
		//this must be performed in a try catch
		spawnNum = rand() % spawnPoints.size();

		//spawn the enemy
		//Enemy *e = new Enemy();
		EntityBuilder::buildEnemy(new Enemy(), "Content/Images/bat.jpg", *targetPtr, new AdvancedAI, spawnPoints.at(spawnNum).position, sf::Vector2f(1.5f, 1.5f));
		
		//restart the clock
		spawnClock.restart();
	}
	
}

void EnemySpawnManager::pauseSpawnManager()
{
	spawnClock.pause();
}

void EnemySpawnManager::resumeSpawnManager()
{
	spawnClock.resume();
}

void EnemySpawnManager::Flush()
{
	spawnPoints.clear();
}

void EnemySpawnManager::Reload(sf::Time spawnRate)
{
	Flush();
	this->spawnRate = spawnRate;
	this->spawnClock.restart();
}