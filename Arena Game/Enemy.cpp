#include "Enemy.h"

Enemy::Enemy()
{
	type  = ENEMY;
	targetPtr = NULL;
	behaviourPtr = NULL;
	health       = 100.0f ;
}

void Enemy::setBehaviour(AI *behaviourPtr)
{
	if(behaviourPtr != NULL)
	{
		//if we had an old behaviourPtr, delete it
		if(this->behaviourPtr != NULL)
			delete this->behaviourPtr;

		this->behaviourPtr = behaviourPtr;
	}
}

void Enemy::setTarget(const Entity *targetPtr)
{
	if(targetPtr != NULL)
		this->targetPtr = targetPtr;
}

Enemy::~Enemy()
{
	delete behaviourPtr;
}

void Enemy::Update()
{
	// the current behaviourPtr computes the move to make,
	if(behaviourPtr != NULL)	
		behaviourPtr->computeMove(*this);

	Character::Update();
}

void Enemy::move(const sf::Vector2f &movement)
{
	setPos(getPos() + movement);
}

void Enemy::initAnimation()
{
	/*NOTE
	*Currently hardcoded to the bat spritesheet.
	*/
	
	//add animations to animation manager
	Animation *down = new Animation();
	down->addRow(sf::Vector2i(0, 0), 4, sf::Vector2i(32, 32), sf::milliseconds(100));
	animationManager.addAnimation("down", down);

	Animation *left = new Animation();
	left->addRow(sf::Vector2i(0, 32), 4, sf::Vector2i(32, 32), sf::milliseconds(100));
	animationManager.addAnimation("left", left);

	Animation *right = new Animation();
	right->addRow(sf::Vector2i(0, 64), 4, sf::Vector2i(32, 32), sf::milliseconds(100));
	animationManager.addAnimation("right", right);

	Animation *up = new Animation();
	up->addRow(sf::Vector2i(0, 96), 4, sf::Vector2i(32, 32), sf::milliseconds(100));
	animationManager.addAnimation("up", up);

	animationManager.playAnimation(*this, "down");
	
}

void Enemy::initAbilities()
{
	//no abilities for now
}

void Enemy::setProjectileList(const std::vector<Entity *> *listPtr)
{
	this->projectileList = listPtr;
}

const std::vector<Entity *> *Enemy::getProjectileList() const
{
	return projectileList;
}
