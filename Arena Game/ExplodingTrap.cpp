#include "ExplodingTrap.h"
#include "EntityBuilder.h"

ExplodingTrap::ExplodingTrap(sf::Time rateOfFireLama)
	: Ability(rateOfFireLama)
{

}

void ExplodingTrap::applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction, const int playerID)
{
	EntityBuilder::buildEntity(new ExplosiveTrap(), "Content/Images/bullet.png", position);
	canFire = false;
}