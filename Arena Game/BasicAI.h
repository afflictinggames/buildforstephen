#ifndef BASIC_AI
#define BASIC_AI

#include "AI.h"
#include "Enemy.h"

class BasicAI : public AI 
{
public:
	BasicAI(void);
	~BasicAI(void);

	virtual void computeMove      (Enemy& enemy) ;
	        void computeDirection ()             ;

private:
	sf::Vector2f  direction ;
	int           up        ;
	int           left      ;
	float         x         ;
	float         y         ;
};
#endif
