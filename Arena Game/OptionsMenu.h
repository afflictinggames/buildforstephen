#ifndef OPTIONSMENU_H_
#define OPTIONSMENU_H_

#include "Menu.h"

class OptionsMenu: public Menu
{
public:

	OptionsMenu();
	~OptionsMenu();
	virtual void initMap(sf::RenderWindow& window, ContentManager &content);
};
#endif