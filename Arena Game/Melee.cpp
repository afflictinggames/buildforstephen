#include "Melee.h"

Melee::Melee()
{
	type = MELEE;
}

Melee::~Melee(void)
{
}

void Melee::Update()
{
	if(pClock.getElapsedTime() >= duration)
		setActive(false);
}

void Melee::setCollisionBox()
{
	collisionBox.left   = (int)(origin.x + offset.x);
	collisionBox.top    = (int)(origin.y + offset.y);
	collisionBox.height = (int)size.y;
	collisionBox.width  = (int)size.x;
}