#include "Menu.h"

Menu::Menu()
{

}

Menu::~Menu()
{

}

void Menu::Load()
{
	
}

void Menu::Update(sf::RenderWindow& window, InputHandler &input)
{
	for(it = menuItems.begin(); it != menuItems.end(); it++)
	{
		if(isInRect(input.getMousePos(window),it->second) &&  input.clicked(window))
		{
			menuValue = it->first;
			break;
		}
	}
}

void Menu::Draw(sf::RenderWindow& window)
{
	for(it = menuItems.begin(); it != menuItems.end(); it++)
	{
		window.draw(background);
		window.draw(it->second);
	}
}

bool Menu::isInRect(sf::Vector2i position, sf::RectangleShape menuItemRect)
{
	return(menuItemRect.getPosition().y < position.y && 
			menuItemRect.getPosition().y + menuItemRect.getSize().y > position.y &&
			menuItemRect.getPosition().x < position.x &&
			menuItemRect.getPosition().x + menuItemRect.getSize().x > position.x);
}


Menu::MenuResult Menu::changeScreen(sf::RenderWindow& window, InputHandler &input)
{
	menuValue = NOTHING;
	while(menuValue == NOTHING)
	{
		Update(window, input);
	}
	return menuValue;
}

void Menu::setBackground(sf::Texture& texture)
{
	background.setTexture(texture);
}