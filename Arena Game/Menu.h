#ifndef MENU_H_
#define MENU_H_

#include "Global.h"
#include "InputHandler.h"
#include "ContentManager.h"

class Menu
{
public:
	Menu();
	virtual ~Menu();

	static const enum MenuResult {PLAY, EXIT, CREDITS, HIGHSCORES, OPTIONS, BACK, RESUME, RESTART, NOTHING};

	virtual void Load();
	virtual void Draw(sf::RenderWindow& window);
	virtual void Update(sf::RenderWindow& window, InputHandler &input);
	virtual void initMap(sf::RenderWindow &window, ContentManager &content) = 0;
	virtual bool isInRect(sf::Vector2i, sf::RectangleShape);
	virtual void setBackground(sf::Texture& texture);

	virtual MenuResult changeScreen(sf::RenderWindow& window, InputHandler &input);
	//const sf::Sprite& getSprite() const {return sprite;}

protected:
	MenuResult menuValue;
	sf::Sprite background;
	std::map<MenuResult, sf::RectangleShape> menuItems;
	std::map<MenuResult,sf::RectangleShape>::iterator it;

};

#endif