#include "BasicAI.h"


BasicAI::BasicAI(void)
{
	computeDirection();
}


BasicAI::~BasicAI(void)
{
}

void BasicAI::computeMove(Enemy& enemy)
{

	enemy.move(direction);
}

void BasicAI::computeDirection()
{
	up   = rand() % 2                                          ;
	left = rand() % 2                                          ;
    x    = rand() % 5                                          ;
	y    = rand() % 5                                          ;

	if(up != 0)
		y = -y;
	if(left != 0)
		x = -x;

	direction = sf::Vector2f(x,y)                              ; 
	direction = VectorFuncs::normalise(direction)              ;
	direction = VectorFuncs::mult(direction,sf::Vector2f(2,2)) ;
}
