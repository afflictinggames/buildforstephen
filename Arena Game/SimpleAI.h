#ifndef SIMPLEAI_H_
#define SIMPLEAI_H_

#include "AI.h"
#include "Enemy.h"
#include "SFML\Graphics.hpp"

class Enemy;

class SimpleAI : public AI
{
	void computeMove(Enemy &enemy);

	sf::Vector2f currentEnemyToTarget;
	int rotatingAngle;
};

#endif