#include "ContentManager.h"

ContentManager::ContentManager()
{

}

ContentManager::~ContentManager()
{
	flush();
}

void ContentManager::addTexture(const std::string filepath)
{
	sf::Texture *texturePtr = new sf::Texture();
	
	//only add to map with loading from file was successful
	if(texturePtr->loadFromFile(filepath))
		textures[filepath] = texturePtr;
}

void ContentManager::removeTexture(const std::string filepath)
{
	//we also want to delete the key, so we must iterate through it
	//to erase it by index
	for(it = textures.begin(); it != textures.end(); it++)
	{
		if(it->first.compare(filepath) == 0)
		{
			textures.erase(it);
			delete it->second;
			break;
		}
	}
}

const sf::Texture *ContentManager::getTexture(std::string filepath)
{
	//we also want to delete the key, so we must iterate through it
	//to erase it by index
	for(it = textures.begin(); it != textures.end(); it++)
	{
		if(it->first.compare(filepath) == 0)
		{
			return it->second;
		}
	}

	std::cerr << "Texture not found at: " << filepath << std::endl;
	return NULL;
}

void ContentManager::flush()
{
	//delete all textures in the map
	for(it = textures.begin(); it != textures.end(); it++)
	{
		delete it->second;
	}
	textures.clear();
}
