#ifndef SPAWNPOINT_H_
#define SPAWNPOINT_H_

#include <SFML\Graphics.hpp>
#include "EntityManager.h"

struct SpawnPoint
{
	sf::Vector2f position;

	SpawnPoint(sf::Vector2f position)
		: position(position)
	{

	}

	void setPosition(sf::Vector2f position)
	{
		this->position = position;
	}

	void spawn(EntityManager &entityManager, Entity &entity)
	{
		entity.setPos(position);
		entityManager.addEntity(&entity);
	}

};

#endif