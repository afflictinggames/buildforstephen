#ifndef TRAP_H_
#define TRAP_H_

#include "AnimatedEntity.h"

class Trap : public AnimatedEntity
{
public:
	Trap();
	~Trap();

	virtual void Update();
	virtual void initAnimation();

protected:

};

#endif
