#ifndef BASICMELEE_H_
#define BASICMELEE_H_

#include "Ability.h"
#include "Melee.h"

class BasicMelee : public Ability
{
public:
	BasicMelee(sf::Time rateOfFireLama);
	~BasicMelee();

	virtual void applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction, const int playerID);
};
#endif