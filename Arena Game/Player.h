#ifndef PLAYER_H_
#define PLAYER_H_

#include "Character.h"
#include "PlayerBars.h"

class Player : public Character
{
public:
	Player();
	~Player();

	virtual void Update();
	virtual void move(const sf::Vector2f &movement);
	virtual void initAnimation();
	virtual void initAbilities();
	void initBars(const sf::Texture &texture);

	void setPlayerID(unsigned int id) { this -> id = id; }
	void setScore   (int score)       { this -> score += score;}
 
	const float getMaxHealth() const { return maxHealth; }
	const unsigned int getPlayerID() const { return id    ;}
	const unsigned int getScore   () const { return score ;}
	PlayerBars getPlayerBars() { return this->playerBars; }

	void autoMove();

private:
	unsigned int id            ;   //Player Identification        //
	bool         movingRight   ;   //If the player is moving right//
	float        maxHealth     ;   //Used for the health bar      //
	long int     score         ;   //The players current score    //
	PlayerBars playerBars;
};
#endif
