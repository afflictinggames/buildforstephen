#ifndef AI_H_
#define AI_H_

class Enemy;

class AI
{
public:
	virtual void computeMove(Enemy &enemy) = 0;
};

#endif
