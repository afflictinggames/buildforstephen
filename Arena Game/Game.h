#ifndef GAME_H_
#define GAME_H_

//Include Menu headers
#include "MainMenu.h"
#include "CreditsMenu.h"
#include "OptionsMenu.h"
#include "HighScoreMenu.h"
#include "SplashScreen.h"
#include "PauseMenu.h"


#include "InputHandler.h"
#include "World.h"

class Game
{
public:
	Game();
	~Game();

	void Start();

private:
	///testtttt
	sf::Clock clock;
	float lastTime;
	//////////////////

	//States of the game
	static const enum GameState{UNINITIALIZED, SHOWSPLASH, PAUSED, 
					SHOWMAINMENU, PLAYING, SHOWOPTIONS, SHOWCREDITS, SHOWHIGHSCORES, EXITING};

	//Checks to see whether we are exiting the game
	bool isExiting();

	//Main game loop
	void gameLoop();

	//Function to show the splash screen
	void showSplashScreen(sf::RenderWindow& window);

	//Function to show the main menu
	void showMainMenu(sf::RenderWindow& window);

	//Function to show the options menu
	void showOptionsMenu(sf::RenderWindow& window);

	//Function to show the high score menu
	void showHighScoreMenu(sf::RenderWindow& window);

	//Function to show the credits menu
	void showCreditsMenu(sf::RenderWindow& window);

	//Function to show the pause menu
	void showPauseMenu(sf::RenderWindow& window);

	//Loads all content creates the window
	void Load();

	//Handles input from player
	void handleInput();

	GameState gameState;
	World currentWorld;

	MainMenu mainMenu;
	SplashScreen splashScreen;
	CreditsMenu creditsMenu;
	OptionsMenu optionsMenu;
	HighScoreMenu highScoreMenu;
	PauseMenu pauseMenu;

	sf::RenderWindow window;

	ContentManager content;
	InputHandler input;
};
#endif