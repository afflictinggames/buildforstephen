#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "Menu.h"

class MainMenu: public Menu
{
public:

	MainMenu();
	~MainMenu();
	virtual void initMap(sf::RenderWindow& window, ContentManager &content);

private:
	
};
#endif