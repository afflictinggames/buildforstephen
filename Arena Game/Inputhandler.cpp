#include "InputHandler.h"

/*
map players to joysticks (and keyboard) here
if(playerIndex = 0)
	keyboard...
	
else
	joystick::isConnected(playerIndex-1, Button)
*/


void InputHandler::bigBootyLoop(sf::RenderWindow& window)
{
	sf::Event myEvent;
	while(window.pollEvent(myEvent))
	{
		switch(myEvent.type)
		{
		case sf::Event::Closed:
			window.close();
			break;
		case sf::Event::KeyPressed:
			//if other keys..
			break;
		case sf::Event::MouseButtonPressed:
			if(myEvent.mouseButton.button == sf::Mouse::Left)
			{
				//left button clicked
				//std::cout << "left clicked" << std::endl;
			}
			break;
		case sf::Event::MouseButtonReleased:
			if(myEvent.mouseButton.button == sf::Mouse::Left)
			{
				//left button released
				//std::cout << "left released" << std::endl;
			}
			break;
		default:
			break;

		}
	}
}

void InputHandler::handleInput(Player &player, const sf::RenderWindow& window)
{
	handlePlayerMovement(player);
	getMousePos(window);
}

void InputHandler::handlePlayerMovement(Player &player)
{
	unsigned int playerID = player.getPlayerID();
	switch(playerID)
	{
	//keyboard, player 1
	case(0):
		//up
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			player.move(sf::Vector2f(0, -player.getSpeed().y));
			player.animate("up", true);
		}
		//down
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			player.move(sf::Vector2f(0, player.getSpeed().y));
			player.animate("down", true);
		}
		//left
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			player.move(sf::Vector2f(-player.getSpeed().x, 0));
				
			//if we are going to move diagonally, just let it play the up or down animation instead
			if(!(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
				player.animate("left", true);
		}
		//right
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			player.move(sf::Vector2f(player.getSpeed().x, 0));
				
			//if we are going to move diagonally, just let it play the up or down animation instead
			if(!(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
				player.animate("right", true);
		}

		if(!(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)
			|| sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
		{
			player.pauseAnimation();
		}
		else
			player.resumeAnimation();
			
		break;
	//gamepad, player 2
	case(1):
		if(sf::Joystick::isConnected(0))
		{
			//get the current values of the x and y axis
			float xVal = sf::Joystick::getAxisPosition(0, sf::Joystick::X);
			float yVal = sf::Joystick::getAxisPosition(0, sf::Joystick::Y);

			//have a threshold, so the axis' aren't too sensitive
			float threshold = 30.0f;

			//if they are below the threshold, they might as well be 0
			if(std::abs(xVal) < threshold) 
				xVal = 0;
			if(std::abs(yVal) < threshold)
				yVal = 0;
				
			//if the player is not moving, don't play an animation
			if(xVal == 0 && yVal == 0)
			{
				player.pauseAnimation();
			}
			else
			{
				//else continue with the animation and move the player
				player.resumeAnimation();
				player.move(sf::Vector2f(player.getSpeed().x * xVal / 100, player.getSpeed().y * yVal / 100));

				//if Y is stronger, play we will be playing the up or down animation
				if(std::abs(yVal) > std::abs(xVal))
				{
					//up
					if(yVal < 0)
						player.animate("up", true);
					//down
					else
						player.animate("down", true);
				}
				else //play left or right animation
				{
					//left
					if(xVal < 0)
						player.animate("left", true);
					//right
					else
						player.animate("right", true);
				}
					
			}
		}
		break;

	default: 
		break;
	}
}

void InputHandler::handlePlayerAttacks(sf::RenderWindow &window, Player &player)
{
	unsigned int playerID = player.getPlayerID();
	switch(playerID)
	{
	//keyboard, player 1
	case 0:
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			player.useAbility(window, "BasicShot",playerID);
		}
		if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			player.useAbility(window, "BasicTrap",playerID);
		}
		break;

	default:
		break;
	}
}

sf::Vector2i InputHandler::getMousePos(const sf::RenderWindow& window)
{
	return VectorFuncs::floatToInt(window.convertCoords(sf::Mouse::getPosition(window)));
}

sf::Vector2i InputHandler::getMouseDirection(const sf::Vector2i v,sf::RenderWindow& window)
{
	return getMousePos(window) - v;
}


bool InputHandler::clicked(sf::RenderWindow &window)
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Left);
}