#include "Camera.h"
#include <sstream>



Camera::Camera(sf::RenderWindow& window)
{
	standard = window.getView();
}

Camera::~Camera()

{

}

void Camera::Update(Player& player,sf::RenderWindow& window)
{
	moveCamera(player,window);
}


void Camera::moveCamera(Player& player,sf::RenderWindow& window)
{
	standard.setCenter(player.getPos());
	window.setView(standard);
}
