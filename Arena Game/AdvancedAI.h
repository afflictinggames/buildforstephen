#ifndef ADVANCEDAI_H_
#define ADVANCEDAI_H_

#include "AI.h"
#include "SFML\Graphics.hpp"


class DodgeStateMachineHeader;
class Enemy; // forward declaration
class Projectile;
class Entity;

class AdvancedAI : public AI
{
public:
	AdvancedAI(); // Constructor
	void computeMove(Enemy &enemy); // the forward declaration makes sure that Enemy has been declared 
	void setDodgeProjectile(Projectile &proj);
	const Projectile* getDodgeProjectile() const;

	bool needToDodge(Projectile *, Enemy &enemy); // checks to see if the bullet is moving withen 10 degrees in our direction
	
	void setSpeed(float);
	float getSpeed();

	void setDodgeSpeed(float);
	float getDodgeSpeed();

	//vector<Entity*> &VectorOfAllProjectilesPtr; // Vector of Entite poitners from the Entity Manager

private:
	bool dodgeState;
	sf::Vector2f dodgeVector;
	std::vector<Entity*> bulletsToDodge;

	bool dodgingProjectile;
	bool dodgeDestination;
	bool reachDestination;
	Projectile* dodgeProjectile; // The projectile that the dodge state will try to dodge
	sf::Vector2f dodgeFinishPos; // The destination of the dodge that will be calculated by the dodge state
	sf::Vector2f currentEnemyToTarget;
	int rotatingAngle;

	float speed;
	float dodgeSpeed;
};

#endif