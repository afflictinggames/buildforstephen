#include "AbilityManager.h"

AbilityManager::AbilityManager()
{

}

AbilityManager::~AbilityManager()
{
	Flush();
}

void AbilityManager::addAbility(const std::string& abilityName, Ability* ability)
{
	if(ability != NULL)
		abilityList[abilityName] = ability;
}

void AbilityManager::removeAbility(const std::string &abilityName)
{
	//only remove the ability if we find it in the list
	if(abilityList.find(abilityName) != abilityList.end())
	{
		abilityList.erase(abilityName);
		delete abilityList[abilityName];
	}
}

void AbilityManager::Update()
{
	for(it = abilityList.begin(); it != abilityList.end(); it++)
	{
		it->second->Update();
	}
}

void AbilityManager::useAbility(const std::string abilityName, const sf::Vector2f &position, const sf::Vector2f &direction,const int playerID)
{
	//only apply the ability if we find it in the list
	if(abilityList.find(abilityName) != abilityList.end() && abilityList[abilityName]->readyForUse())
	{
		abilityList[abilityName]->applyAbility(position, direction,playerID);
	}
}

void AbilityManager::Flush()
{
	for(it = abilityList.begin(); it != abilityList.end(); it++)
	{
		delete it->second;
	}
	abilityList.clear();
}

void AbilityManager::pauseAbilities()
{
	for(it = abilityList.begin(); it != abilityList.end(); it++)
	{
		it->second->pauseAbility();
	}
}

void AbilityManager::resumeAbilities()
{
	for(it = abilityList.begin(); it != abilityList.end(); it++)
	{
		it->second->resumeAbility();
	}
}