#ifndef ABILITYMANAGER_H_
#define ABILITYMANAGER_H_

#include "Ability.h"
#include "Entity.h"

class AbilityManager
{
public:
	AbilityManager();
	~AbilityManager();

	//Adds an Ability to the manager
	void addAbility(const std::string &abilityName, Ability* ability);

	//Removes an Ability from the manager
	void removeAbility(const std::string &abilityName);

	//Updates the targets list of abilities
	void Update();

	//Uses particular ability
	void useAbility(const std::string abilityName, const sf::Vector2f &position, const sf::Vector2f &direction,const int playerID);

	//Deletes all abilities from the manager and clears the map
	void Flush();

	//Pauses abilities
	void pauseAbilities();

	//Resumes Abilities
	void resumeAbilities();

private:
	std::map<std::string, Ability*>::const_iterator it;
	std::map<std::string, Ability*> abilityList;
};

#endif //ABILITYMANAGER