#include "Ability.h"

Ability::Ability(sf::Time rateOfFireLama)
	: rateOfFireLama(rateOfFireLama), canFire(true)
{
	pClock.restart();
}


Ability::~Ability()
{

}

void Ability::Update()
{
	if(pClock.getElapsedTime() >= rateOfFireLama)
	{
		canFire = true;
		pClock.restart();
	}
}

void Ability::pauseAbility()
{
	pClock.pause();
}

void Ability::resumeAbility()
{
	pClock.resume();
}