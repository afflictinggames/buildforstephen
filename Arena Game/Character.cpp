#include "Character.h"


Character::Character()
{
}

Character::~Character(void)
{
}

void Character::Update()
{
	abilityManager.Update();
	AnimatedEntity::Update();

	if(health <= 0)
		setActive(false);
}

void Character::useAbility(sf::RenderWindow &window, std::string abilityName,const int ID)
{
	abilityManager.useAbility(abilityName, this->getPos(), (window.convertCoords(sf::Mouse::getPosition(window))) - this->getPos(),ID);
}

void Character::pauseAnimation()
{
	animationManager.pauseCurrentAnimation();
}

void Character::resumeAnimation()
{
	animationManager.resumeCurrentAnimation();
}

void Character::pauseAbilities()
{
	abilityManager.pauseAbilities();
}

void Character::resumeAbilities()
{
	abilityManager.resumeAbilities();
}