#ifndef ABILITY_H_
#define ABILITY_H_

#include "PausableClock.h"
#include "SFML\Graphics.hpp"

//forward declaration test
class EntityBuilder;

class Ability
{
public:

	Ability(sf::Time rateOfFireLama);
	~Ability();
	
	void Update();
	virtual void applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction,const int playerID)=0;
	const bool readyForUse() { return canFire; }
	void pauseAbility();
	void resumeAbility();

protected:
	PausableClock pClock;
	sf::Time rateOfFireLama;
	bool canFire;
};
#endif
