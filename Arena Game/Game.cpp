#include "Game.h"

Game::Game()
	: currentWorld(content)
{
	gameState = UNINITIALIZED;
	Load();
}

Game::~Game()
{

}

void Game::Load()
{
	//entities
	content.addTexture("Content/Images/Wall.jpg");
	content.addTexture("Content/Images/player.png");
	content.addTexture("Content/Images/bat.jpg");
	content.addTexture("Content/Images/bullet.png");
	content.addTexture("Content/Images/trap1.png");
	content.addTexture("Content/Images/trap2.png");
	content.addTexture("Content/Images/trap3.png");

	//backgrounds
	content.addTexture("Content/Images/Background.png");
	content.addTexture("Content/Images/arena2.png");

	//menu
	content.addTexture("Content/Images/splashScreen.png");
	content.addTexture("Content/Images/playButton.png");
	content.addTexture("Content/Images/optionsButton.png");
	content.addTexture("Content/Images/exitButton.png");

	window.setFramerateLimit(60);
	//window.create(sf::VideoMode::getDesktopMode(),"Hexagonal Cat", sf::Style::Fullscreen);
	window.create(sf::VideoMode(800, 600),"Hexagonal Cat");

	//Initialize the menus
	mainMenu.initMap(window, content);
	creditsMenu.initMap(window, content);
	optionsMenu.initMap(window, content);
	highScoreMenu.initMap(window, content);
	pauseMenu.initMap(window,content);
}

void Game::Start()
{
	if(gameState != UNINITIALIZED)
		return;
	
	gameState = Game::SHOWSPLASH;

	while(!isExiting())
	{
		gameLoop();
	}

	window.close();
}

bool Game::isExiting()
{
	if(gameState == Game::EXITING) 
		return true;
	else 
		return false;
}

void Game::gameLoop()
{
	input.bigBootyLoop(window);
	handleInput();
	switch(gameState)
	{
	case Game::SHOWMAINMENU:
		{
			window.clear(sf::Color(100, 100, 100));
			mainMenu.Draw(window);
			window.display();

			showMainMenu(window);
			break;
		}
	case Game::SHOWSPLASH:
		{
			showSplashScreen(window);
			break;
		}
	case Game::SHOWCREDITS:
		{
			window.clear(sf::Color(100, 100, 100));
			optionsMenu.Draw(window);
			window.display();

			showOptionsMenu(window);
			break;
		}
	case Game::SHOWOPTIONS:
		{
			window.clear(sf::Color(100, 100, 100));
			creditsMenu.Draw(window);
			window.display();

			showCreditsMenu(window);
			break;
		}
	case Game::SHOWHIGHSCORES:
		{
			window.clear(sf::Color(100, 100, 100));
			highScoreMenu.Draw(window);
			window.display();

			showHighScoreMenu(window);
			break;
		}
	case Game::PLAYING:
		{
			window.clear(sf::Color::White);
			//update and draw world
			currentWorld.Update(window);
			currentWorld.Draw(window);

			/////////FPS TEST///////////
			float currentTime = clock.restart().asSeconds();
            int fps = 1.f / (lastTime);
            lastTime = currentTime;
			////////////////////////////////

			window.display();

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				gameState = Game::EXITING;

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
				gameState = Game::PAUSED;

			break;
		}
	case Game::PAUSED:
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				gameState = Game::PLAYING;
		
			window.clear(sf::Color::White);
			pauseMenu.Draw(window);
			currentWorld.pause(window);
			window.display();

			showPauseMenu(window);
			break;
		}
	default:
		break;
	}
}

void Game::showSplashScreen(sf::RenderWindow& window)
{
	splashScreen.Show(window, content, input);
	gameState = Game::SHOWMAINMENU;
}

void Game::showMainMenu(sf::RenderWindow& window)
{
	MainMenu::MenuResult value = mainMenu.changeScreen(window, input);

	switch(value)
	{
	case MainMenu::EXIT:
		gameState = Game::EXITING;
		break;
	case MainMenu::PLAY:
		gameState = Game::PLAYING;
		currentWorld.Load(content,window);
		break;
	case MainMenu::OPTIONS:
		gameState = Game::SHOWOPTIONS;
		break;
	case MainMenu::HIGHSCORES:
		gameState = Game::SHOWHIGHSCORES;
		break;
	case MainMenu::CREDITS:
		gameState = Game::SHOWCREDITS;
		break;
	default:
		break;
	}
}

void Game::showCreditsMenu(sf::RenderWindow& window)
{
	CreditsMenu::MenuResult value = creditsMenu.changeScreen(window, input);
	switch(value)
	{
	case CreditsMenu::BACK:
		gameState = Game::SHOWMAINMENU;
		break;
	default:
		break;
	}
}

void Game::showOptionsMenu(sf::RenderWindow& window)
{
	OptionsMenu::MenuResult value = optionsMenu.changeScreen(window, input);
	switch(value)
	{
	case OptionsMenu::BACK:
		gameState = Game::SHOWMAINMENU;
		break;
	default:
		break;
	}
}

void Game::showHighScoreMenu(sf::RenderWindow& window)
{
	HighScoreMenu::MenuResult value = highScoreMenu.changeScreen(window, input);
	switch(value)
	{
	case HighScoreMenu::BACK:
		gameState = Game::SHOWMAINMENU;
		break;
	default:
		break;
	}
}

void Game::showPauseMenu(sf::RenderWindow& window)
{
	PauseMenu::MenuResult value = pauseMenu.changeScreen(window, input);
	switch(value)
	{
	case PauseMenu::RESUME:

		currentWorld.resume(window);
		gameState = Game::PLAYING;

		break;
	case PauseMenu::RESTART:

		currentWorld.resume(window);
		currentWorld.Reload(content, window);
		gameState = Game::PLAYING;

	default:
		break;
	}
}

void Game::handleInput()
{
	const std::vector<Entity*> players = *currentWorld.getPlayers();
	for(unsigned int i = 0; i < players.size(); i++)
	{
		input.handlePlayerMovement(*dynamic_cast<Player*>(players.at(i)));
		input.handlePlayerAttacks(window, *dynamic_cast<Player*>(players.at(i)));
	}
}
