#include "SpreadShot.h"
#include "EntityBuilder.h"

SpreadShot::SpreadShot(sf::Time rateOfFireLama)
	: Ability(rateOfFireLama)
{

}

SpreadShot::~SpreadShot()
{

}

void SpreadShot::applyAbility(const sf::Vector2f &position, const sf::Vector2f &direction,const int playerID)
{
	EntityBuilder::buildProjectile(new Projectile(), "Content/Images/bullet.png", position, direction, sf::Vector2f(5, 5), playerID);

	for(int i = 1; i < 3; i ++)
	{
		EntityBuilder::buildProjectile(new Projectile(), "Content/Images/bullet.png", position, VectorFuncs::rotate(direction,10.0f*i), sf::Vector2f(5.0f, 5.0f), playerID);
		EntityBuilder::buildProjectile(new Projectile(), "Content/Images/bullet.png", position, VectorFuncs::rotate(direction,-10.0f*i), sf::Vector2f(5.0f, 5.0f), playerID);
	}
	canFire = false;
}
