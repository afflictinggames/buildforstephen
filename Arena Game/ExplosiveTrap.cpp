#include "ExplosiveTrap.h"

ExplosiveTrap::ExplosiveTrap()
{
	type = EXPLOSIVE_TRAP;
	initAnimation();
}

ExplosiveTrap::~ExplosiveTrap()
{
	/*
	Projectile* topLeft = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(-100,-100), sf::Vector2f(5,5));
	Projectile* topMid = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(0,-100), sf::Vector2f(5,5));
	Projectile* topRight = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(100,-100), sf::Vector2f(5,5));
	Projectile* midLeft = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(-100,0), sf::Vector2f(5,5));
	Projectile* midRight = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(100,0), sf::Vector2f(5,5));
	Projectile* bottomLeft = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(-100,100), sf::Vector2f(5,5));
	Projectile* bottomMid = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(0,100), sf::Vector2f(5,5));
	Projectile* bottomRight = new Projectile(ContentManager::projectileTexture , getPos(), sf::Vector2f(100,100), sf::Vector2f(5,5));
	EntityManager::addEntity(topLeft);
	EntityManager::addEntity(topMid);
	EntityManager::addEntity(topRight);
	EntityManager::addEntity(midLeft);
	EntityManager::addEntity(midRight);
	EntityManager::addEntity(bottomLeft);
	EntityManager::addEntity(bottomMid);
	EntityManager::addEntity(bottomRight);
	*/
}

void ExplosiveTrap::initAnimation()
{
	//add animations to animation manager
	Animation *trap = new Animation();
	trap->addFrame(sf::seconds(1), sf::IntRect(0, 0, 20, 20));
	animationManager.addAnimation("trap", trap);

	animationManager.playAnimation(*this, "trap");
}