#ifndef CHARACTER_H_
#define CHARACTER_H_

#include "AnimatedEntity.h"
#include "AbilityManager.h"

class Character : public AnimatedEntity
{
public:
	Character();
	~Character();

	virtual void Update();

	//move relative to current position
	//later this will be replaced by AI movement, instead of just a vector
	virtual void move(const sf::Vector2f &movement) = 0;

	//animation
	//inherited from animated entity: virtual void initAnimation() = 0;

	virtual void pauseAnimation();
	virtual void resumeAnimation();

	void pauseAbilities();
	void resumeAbilities();

	void useAbility(sf::RenderWindow &window, std::string abilityName,const int ID);

	virtual void initAbilities() = 0;

	float getHealth ()             {return health         ;}
	void setHealth  (float health) {this->health = health ;}

protected:
	AbilityManager abilityManager ;
	float          health         ;
};
#endif
