#ifndef INPUT_HANDLER_H_
#define INPUT_HANDLER_H_

#include "Global.h"
#include "Player.h"

class InputHandler
{
public:
	//Handles input
	void handleInput(Player &player, const sf::RenderWindow& window);

	//Handles player movement
	void handlePlayerMovement(Player &player);

	//Handles player attacks
	void handlePlayerAttacks(sf::RenderWindow &window, Player &player);

	//Gets the mouse position
	sf::Vector2i getMousePos(const sf::RenderWindow& window);

	//Gets the direction between a vector and a mouse
	sf::Vector2i getMouseDirection(const sf::Vector2i v,sf::RenderWindow& window);

	//Checks to see whether the left mouse button was clicked
	bool clicked(sf::RenderWindow &window);

	//Main loop
	void bigBootyLoop(sf::RenderWindow &window);
};
#endif