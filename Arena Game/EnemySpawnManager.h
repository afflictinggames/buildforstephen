#ifndef ENEMYSPAWNMANAGER_H_
#define ENEMYSPAWNMANAGER_H_

#include <ctime>
#include <SFML\System\Time.hpp>
#include "SpawnPoint.h"
#include "PausableClock.h"
#include "EntityManager.h"
#include "Player.h"
#include "Enemy.h"
#include "ContentManager.h"
#include "EntityBuilder.h"


class EnemySpawnManager
{
public:
	EnemySpawnManager(sf::Time spawnRate = sf::seconds(5));
	~EnemySpawnManager(void);

	//adds a spawn point to the list
	void addSpawnPoint(SpawnPoint spawnPoint);

	//keeps the spawn manager updated
	void Update(const std::vector<Entity*> &targetPtrs);

	//Pauses the spawn clock
	void pauseSpawnManager();

	//Resumes the spawn clock
	void resumeSpawnManager();

	//Flushes the spawn points
	void Flush();

	//Reloads the spawn manager
	void Reload(sf::Time spawnRate = sf::seconds(2));

private:
	std::vector<SpawnPoint> spawnPoints;
	PausableClock spawnClock;
	sf::Time spawnRate;

	Entity *targetPtr;
	unsigned int spawnNum;
};

#endif

