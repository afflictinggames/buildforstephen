#ifndef PAUSEMENU_H_
#define PAUSEMENU_H_

#include "Menu.h"

class PauseMenu: public Menu
{
public:

	PauseMenu();
	~PauseMenu();
	virtual void initMap(sf::RenderWindow& window, ContentManager &content);
};
#endif