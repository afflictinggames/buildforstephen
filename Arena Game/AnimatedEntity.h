#ifndef ANIMATEDENTITY
#define ANIMATEDENTITY

#include "Entity.h"
#include "IUpdatable.h"
#include "AnimationManager.h"

class AnimatedEntity : public Entity, public IUpdatable
{
public:
	AnimatedEntity();
	~AnimatedEntity();

	virtual void Update();

	//animation
	virtual void initAnimation() = 0;

	virtual void animate(const std::string &name, const bool loop = false);
	void pauseAnimation();
	void resumeAnimation();

protected:
	AnimationManager animationManager;
};

#endif

