#include "Trap.h"

Trap::Trap()
{
	type = TRAP;
	initAnimation();
}

Trap::~Trap()
{

}

void Trap::Update()
{
	AnimatedEntity::Update();
}

void Trap::initAnimation()
{
	//add animations to animation manager
	Animation *trap = new Animation();
	trap->addFrame(sf::seconds(1), sf::IntRect(0, 0, 20, 20));
	animationManager.addAnimation("trap", trap);

	animationManager.playAnimation(*this, "trap");
}