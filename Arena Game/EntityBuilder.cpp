#include "EntityBuilder.h"

ContentManager *EntityBuilder::contentManagerPtr;
EntityManager *EntityBuilder::entityManagerPtr;

void EntityBuilder::init(ContentManager *contentManagerPtr, EntityManager *entityManagerPtr)
{
	EntityBuilder::contentManagerPtr = contentManagerPtr;
	EntityBuilder::entityManagerPtr = entityManagerPtr;
}

void EntityBuilder::release()
{

}

void EntityBuilder::buildEntity(Entity *entityPtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f speed)
{
	entityPtr->Load(*contentManagerPtr->getTexture(textureFilePath));
	entityPtr->setPos(position);
	entityPtr->setSpeed(speed);
	entityManagerPtr->addEntity(entityPtr);
}

void EntityBuilder::buildProjectile(Projectile *projectilePtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f direction, const sf::Vector2f speed, const int playerID)
{
	projectilePtr    -> Load        (*contentManagerPtr -> getTexture(textureFilePath)) ;
	projectilePtr    -> setPos      (position)                                          ;
	projectilePtr    -> setVelocity (direction, speed)                                  ;
	projectilePtr    -> setPlayerID (playerID)                                          ;
	entityManagerPtr -> addEntity   (projectilePtr)                                     ;
}

void EntityBuilder::buildEnemy(Enemy *enemyPtr, const std::string textureFilePath, const Entity &target, AI *behaviourPtr, const sf::Vector2f position, const sf::Vector2f speed)
{
	enemyPtr->Load(*contentManagerPtr->getTexture(textureFilePath));
	enemyPtr->setPos(position);
	enemyPtr->setSpeed(speed);
	enemyPtr->setTarget(&target);
	enemyPtr->setBehaviour(behaviourPtr);
	enemyPtr->initAnimation();
	enemyPtr->initAbilities();

	enemyPtr->setProjectileList(entityManagerPtr->getVector(PROJECTILE));

	//messing around with this
	entityManagerPtr->addEntity(enemyPtr);
	//entityManagerPtr->addUpdatable(enemyPtr, ENEMY);
}

void EntityBuilder::buildPlayer(Player *playerPtr, const std::string textureFilePath, const sf::Vector2f position, const sf::Vector2f speed, unsigned int id)
{
	playerPtr->Load(*contentManagerPtr->getTexture(textureFilePath));
	playerPtr->setPos(position);
	playerPtr->setSpeed(speed);
	playerPtr->initAnimation();
	playerPtr->initAbilities();
	playerPtr->setPlayerID(id);
	entityManagerPtr->addEntity(playerPtr);
}

void EntityBuilder::buildMelee(Melee *meleePtr, const sf::Vector2f origin, const sf::Vector2f offset, const sf::Vector2f size, const sf::Time duration)
{
	meleePtr->setOrigin(origin);
	meleePtr->setOffSet(offset);
	meleePtr->setSize(size);
	meleePtr->setDuration(duration);
	meleePtr->setCollisionBox();
	entityManagerPtr->addEntity(meleePtr);
}