#ifndef PLAYERBARS_H_
#define PLAYERBARS_H_

#include "Global.h"

struct PlayerBars
{
	PlayerBars(){}

	PlayerBars(sf::RectangleShape health, sf::RectangleShape energy)
		:health(health), energy(energy)
	{

	}

	sf::RectangleShape health;
	sf::RectangleShape energy;
};

#endif