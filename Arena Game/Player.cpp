#include "Player.h"


#include "BasicShot.h"
#include "BasicTrap.h"
#include "SpreadShot.h"
#include "ExplodingTrap.h"
#include "BasicMelee.h"

Player::Player()
{
	type      = PLAYER ;
	health    = 100.0f ;
	maxHealth = health ;
	score     = 0      ;
}

Player::~Player()
{

}

void Player::Update()
{
	//autoMove();
	Character::Update();


}

void Player::move(const sf::Vector2f &movement)
{
	setPos(getPos() + movement);
}

void Player::initAnimation()
{
	//add animations to animation manager
	Animation *down = new Animation();
	down->addRow(sf::Vector2i(0, 0), 4, sf::Vector2i(32, 49), sf::milliseconds(100));
	animationManager.addAnimation("down", down);

	Animation *left = new Animation();
	left->addRow(sf::Vector2i(0, 49), 4, sf::Vector2i(32, 49), sf::milliseconds(100));
	animationManager.addAnimation("left", left);

	Animation *right = new Animation();
	right->addRow(sf::Vector2i(0, 98), 4, sf::Vector2i(32, 49), sf::milliseconds(100));
	animationManager.addAnimation("right", right);

	Animation *up = new Animation();
	up->addRow(sf::Vector2i(0, 147), 4, sf::Vector2i(32, 49), sf::milliseconds(100));
	animationManager.addAnimation("up", up);

	animationManager.playAnimation(*this, "down");
}

void Player::initAbilities()
{
	abilityManager.addAbility("BasicShot", new BasicShot(sf::milliseconds(200)));
	abilityManager.addAbility("SpreadShot", new SpreadShot(sf::milliseconds(200)));
	abilityManager.addAbility("BasicTrap", new BasicTrap(sf::milliseconds(1000)));
	abilityManager.addAbility("ExplosiveTrap", new ExplodingTrap(sf::milliseconds(1000)));
	//abilityManager.addAbility("BasicMelee" , new BasicMelee(sf::milliseconds(300)));
}

void Player::initBars(const sf::Texture &texture)
{
	sf::RectangleShape health;
	health.setSize(sf::Vector2f(200.0f, 50.0f));
	health.setTexture(&texture);
	health.setPosition(100,100);

	sf::RectangleShape energy;
	energy.setSize(sf::Vector2f(200.0f, 50.0f));
	energy.setTexture(&texture);
	energy.setPosition(100,200);

	playerBars.health = health;
	playerBars.energy = energy;
}

void Player::autoMove()
{
	if(getPos().x > 700)
		movingRight = false;
	else if(getPos().x < 100)
		movingRight = true;

	if(movingRight)
		move(sf::Vector2f(speed.x, 0));
	else
		move(sf::Vector2f(-speed.x, 0));
}