#include "MainMenu.h"

MainMenu::MainMenu()
{

}

MainMenu::~MainMenu()
{

}

void MainMenu::initMap(sf::RenderWindow &window, ContentManager &content)
{
	//hardcoded positions
	sf::RectangleShape playButton;
	playButton.setSize(sf::Vector2f(400.0f, 200.0f));
	playButton.setTexture(content.getTexture("Content/Images/playButton.png"));
	playButton.setPosition(window.getSize().x / 2 - playButton.getSize().x / 2, 0);

	sf::RectangleShape optionsButton;
	optionsButton.setSize(sf::Vector2f(400.0f, 200.0f));
	optionsButton.setTexture(content.getTexture("Content/Images/optionsButton.png"));
	optionsButton.setPosition(window.getSize().x / 2 - optionsButton.getSize().x / 2, 200);

	sf::RectangleShape highScoreButton;
	highScoreButton.setSize(sf::Vector2f(400.0f,200.0f));
	highScoreButton.setTexture(content.getTexture("Content/Images/exitButton.png"));
	highScoreButton.setPosition(window.getSize().x / 2 - highScoreButton.getSize().x / 2, 600);

	sf::RectangleShape creditsButton;
	creditsButton.setSize(sf::Vector2f(400.0f,200.0f));
	creditsButton.setTexture(content.getTexture("Content/Images/exitButton.png"));
	creditsButton.setPosition(100, 100);

	sf::RectangleShape exitButton;
	exitButton.setSize(sf::Vector2f(400.0f,200.0f));
	exitButton.setTexture(content.getTexture("Content/Images/exitButton.png"));
	exitButton.setPosition(window.getSize().x / 2 - exitButton.getSize().x / 2, 400);

	

	menuItems[PLAY] = playButton;
	menuItems[OPTIONS] = optionsButton;
	menuItems[CREDITS] = creditsButton;
	menuItems[HIGHSCORES] = highScoreButton;
	menuItems[EXIT] = exitButton;
}