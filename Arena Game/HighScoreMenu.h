#ifndef HIGHSCOREMENU_H_
#define HIGHSCOREMENU_H_

#include "Menu.h"

class HighScoreMenu: public Menu
{
public:

	HighScoreMenu();
	~HighScoreMenu();
	virtual void initMap(sf::RenderWindow& window, ContentManager &content);
};
#endif