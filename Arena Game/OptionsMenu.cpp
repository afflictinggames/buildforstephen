#include "OptionsMenu.h"

OptionsMenu::OptionsMenu()
{

}

OptionsMenu::~OptionsMenu()
{

}

void OptionsMenu::initMap(sf::RenderWindow &window, ContentManager &content)
{
	//hardcoded positions
	sf::RectangleShape backButton;
	backButton.setSize(sf::Vector2f(400.0f, 200.0f));
	backButton.setTexture(content.getTexture("Content/Images/playButton.png"));
	backButton.setPosition(window.getSize().x / 2 - backButton.getSize().x / 2, 400);
	menuItems[BACK] = backButton;
}
