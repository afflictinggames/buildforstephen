#include "Game.h"
//uncomment this if we want to detect memory leaks
//#include <vld.h>


int main(int argc, char** argv)
{
	Game game;
	game.Start();

	/*
	* If the annoying input bug returns, uncomment this line
	* to see if any error message is printed out after the window closes
	*/
	//system("pause");

	return 0;
}

/*
So, let's talk about this slow down.
Without a frame limit
It randomly drops to around 10 - 40 fps

Observations:
Believe it's the entity manager... but granted, this is a huge part of our game.
Therefore, it could be occurring further down.

Problem is still there after removing:
collision
entity manager update completely
animation
input
*/
