#ifndef CREDITSMENU_H_
#define CREDITSMENU_H_

#include "Menu.h"

class CreditsMenu: public Menu
{
public:

	CreditsMenu();
	~CreditsMenu();
	virtual void initMap(sf::RenderWindow& window, ContentManager &content);
};
#endif