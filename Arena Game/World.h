#ifndef WORLD_H_
#define WORLD_H_

#include "Global.h"
#include "Camera.h"
#include "EntityManager.h"
#include "ContentManager.h"
#include "Player.h"
#include "Enemy.h"
#include "Wall.h"
#include "EnemySpawnManager.h"
#include "EntityBuilder.h"

#include "HUD.h"

class World
{
public:
	World(ContentManager &content);
	~World(void);

	//Updates
	void Update(sf::RenderWindow& window);

	//Loads entities into the world
	void Load(ContentManager &content,sf::RenderWindow& window);
	
	//Reloads everything in the world
	void Reload(ContentManager &content, sf::RenderWindow& window);

	//Draws everything in the world
	void Draw(sf::RenderWindow& window);

	//Pauses everything on screen
	void pause(sf::RenderWindow& window);

	//Resume everything on screen
	void resume(sf::RenderWindow& window);

	//Returns the player vector
	const std::vector<Entity*> *getPlayers();

	//
	std::vector<Entity*> *getHUDPlayers();

private:
	EntityManager       entityManager     ;
	sf::Sprite          worldSprite       ;
	EnemySpawnManager   enemySpawnManager ;
	Camera            * camera            ;
	Player            *  p1               ;
	std::vector<Entity*> pauseList;
	HUD hud;
};

#endif
