#include "EntityManager.h"


EntityManager::EntityManager()
{

}

EntityManager::~EntityManager()
{
	Flush();
}

void EntityManager::addEntity(Entity *entityPtr)
{
	if(entityPtr != NULL)
	{
		danYoureOffTheTeam[entityPtr->getType()].push_back(entityPtr);

		//check to see if it's an updatable entity
		IUpdatable *ptr = dynamic_cast<IUpdatable*>(entityPtr);
		if(ptr != NULL)
			addUpdatable(ptr, entityPtr->getType());
	}
}

void EntityManager::addUpdatable(IUpdatable *updatableEntityPtr, TYPE type)
{
	if(updatableEntityPtr != NULL)
		danIsSilly[type].push_back(updatableEntityPtr);
}

void EntityManager::Flush()
{
	for(i = 0 ; i < NUM_TYPES ; i++)
	{
		for(j = 0; j < danYoureOffTheTeam[i].size(); j++)
		{
			delete danYoureOffTheTeam[i][j];
		}
		danYoureOffTheTeam[i].clear();
		danIsSilly[i].clear();
	}
}

bool EntityManager::isOnScreen(const Entity &entity, const sf::RenderWindow& window) const 
{
	sf::Vector2f pos        = entity.getPos()              ;
	sf::Vector2f viewCenter = window.getView().getCenter() ;
	sf::Vector2u windowSize = window.getSize()             ;

	if(pos.x > viewCenter.x - windowSize.x / 2 && pos.x < viewCenter.x + windowSize.x/2
		&& pos.y > viewCenter.y - windowSize.y / 2 && pos.y < viewCenter.y + windowSize.y/2)
	{
		return true ;
	}

	return false;
}

void EntityManager::Draw(sf::RenderWindow &window)
{
	for(i = 0 ; i < NUM_TYPES ; i++)
	{
		for(j = 0; j < danYoureOffTheTeam[i].size(); j++)
		{
			Entity *entityPtr = danYoureOffTheTeam[i][j];
			////////////////////// TEST //////////////////////////////////
			if(i == PROJECTILE)
			{
				if(!isOnScreen(*entityPtr, window))
				{
					entityPtr->setActive(false);
				}
			}
			//////////////////////////////////////////////////////////////

			/*before we draw this entity, first we need to see if 
			*it is active. If it is not active, delete it from the lists
			*/
			if(!entityPtr->isActive())
			{
				eraseInUpdatable(entityPtr);
				danYoureOffTheTeam[i].erase(danYoureOffTheTeam[i].begin() + j);
				delete entityPtr;
				j--;
			}
			else if(isOnScreen(*entityPtr, window))
			{
				/*if we got in here, the entity is still active and it
				*is currently on the screen, so draw it
				*/
				entityPtr->Draw(window);
			}
			
		}
	}
}

void EntityManager::Update(const sf::RenderWindow &window)
{
	////TEST/////////////////////////////////////////////////////
	/*
	*Probably should put this somewhere else 
	*but I need access to players vector in 
	*collision
	*/
	collision.setPlayers(*getVector(PLAYER));
	/////////////////////////////////////////////////////////////


	//update all entities
	for(i = 0 ; i < NUM_TYPES ; i++)
	{
		for(j = 0; j < danIsSilly[i].size(); j++)
		{
			danIsSilly[i][j]->Update();
		}
	}
	
	checkCollision(window);
}

void EntityManager::eraseInUpdatable(Entity *entityPtr)
{
	/*
	*try to cast it to type updatable. If the cast is unsuccessful, then
	*the entity is not in the updatable list
	*/
	IUpdatable *ptr = dynamic_cast<IUpdatable*>(entityPtr);
	if(ptr != NULL)
	{
		//find which vector we need to iterate through to find it
		TYPE type = entityPtr->getType();
		//find the entity by reference and remove it
		for(unsigned int j = 0; j < danIsSilly[type].size(); j++)
		{
			if(danIsSilly[type][j] == ptr)
			{
				danIsSilly[type].erase(danIsSilly[type].begin() + j);
				//we found it, so we can break out since there is no need to keep looking
				break;
			}
		}
	}
}

void EntityManager::checkCollision(const sf::RenderWindow& window)
{
	collision.collision(danYoureOffTheTeam[MELEE],danYoureOffTheTeam[ENEMY],1);
	collision.collision(danYoureOffTheTeam[ENEMY],danYoureOffTheTeam[PROJECTILE],2);
	collision.collision(danYoureOffTheTeam[WALL],danYoureOffTheTeam[PROJECTILE],2);
	collision.collision(danYoureOffTheTeam[ENEMY],danYoureOffTheTeam[PLAYER],3);
	collision.collision(danYoureOffTheTeam[PLAYER],danYoureOffTheTeam[WALL],4);
	collision.collision(danYoureOffTheTeam[ENEMY],danYoureOffTheTeam[WALL],4);
}

const vector<Entity*> *EntityManager::getVector(TYPE type) const
{
	if(type != NUM_TYPES)
	{
		return &danYoureOffTheTeam[type];
	}
	return NULL;
}

vector<Entity*> *EntityManager::getPlayerVector()
{
	return &danYoureOffTheTeam[PLAYER];
}

const vector<Entity*> *EntityManager::getOnScreenEntities(sf::RenderWindow& window, std::vector<Entity*> &entitiesOnScreen)
{
	for(i = 0 ; i < NUM_TYPES ; i++)
	{
		for(j = 0; j < danYoureOffTheTeam[i].size(); j++)
		{
			if(isOnScreen(*danYoureOffTheTeam[i][j] , window))
			{
				entitiesOnScreen.push_back(danYoureOffTheTeam[i][j]);
			}
		}
	}
	return &entitiesOnScreen;
}