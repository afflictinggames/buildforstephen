#include "World.h"

World::World(ContentManager &content)
	: enemySpawnManager(sf::seconds(5))
{	
	EntityBuilder::init(&content, &entityManager);
}


World::~World(void)
{
	delete this->camera;
}

void World::Update(sf::RenderWindow& window)
{
	enemySpawnManager.Update(*getPlayers());
	entityManager.Update(window);
	hud.Update();
	camera -> Update(*p1,window);
}

void World::Load(ContentManager &content,sf::RenderWindow& window)
{
	worldSprite.setTexture(*content.getTexture("Content/Images/arena2.png"));
	worldSprite.setPosition(0,0);

	/////TEST//////////////////////////////////////////////////////////////////
	p1 = new Player();  
	EntityBuilder::buildPlayer(p1, "Content/Images/player.png", sf::Vector2f(300, 100), sf::Vector2f(3, 3), 0);

	Wall *wall = new Wall();
	EntityBuilder::buildEntity(wall, "Content/Images/Wall.jpg", sf::Vector2f(300, 300));

	camera = new Camera(window); 
	/////////////////////////////////////////////////////////////////////////////
	
	//Initialize spawn points
	//left of screen
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(50, 250)));
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(50, 450)));
	//above screen
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(250, 50)));
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(450, 50)));
	//right of screen
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(750 ,200)));
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(750, 400)));
	//below screen
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(250, 550)));
	enemySpawnManager.addSpawnPoint(SpawnPoint(sf::Vector2f(450, 550)));
	
	hud.init(getHUDPlayers());
}

void World::Reload(ContentManager& content, sf::RenderWindow& window)
{
	entityManager.Flush();
	enemySpawnManager.Reload();
	Load(content, window);
	hud.Flush();
}

void World::Draw(sf::RenderWindow &window)
{
	window.draw(worldSprite);
	entityManager.Draw(window);
	hud.Draw(window);
}

const std::vector<Entity*> *World::getPlayers()
{
	return entityManager.getVector(PLAYER);
}

std::vector<Entity*> *World::getHUDPlayers()
{
	return entityManager.getPlayerVector();
}

void World::pause(sf::RenderWindow& window)
{
	pauseList.clear();
	entityManager.getOnScreenEntities(window, pauseList);
	
	enemySpawnManager.pauseSpawnManager();
	for(unsigned int i = 0; i < pauseList.size(); i++)
	{
		if(dynamic_cast<Character*>(pauseList.at(i)) != NULL)
		{
			dynamic_cast<Character*>(pauseList.at(i))->pauseAnimation();
			dynamic_cast<Character*>(pauseList.at(i))->pauseAbilities();
		}
	}
}

void World::resume(sf::RenderWindow& window)
{
	enemySpawnManager.resumeSpawnManager();
	for(unsigned int i = 0; i < pauseList.size(); i++)
	{
		if(dynamic_cast<Character*>(pauseList.at(i)) != NULL)
		{
			dynamic_cast<Character*>(pauseList.at(i))->resumeAnimation();
			dynamic_cast<Character*>(pauseList.at(i))->resumeAbilities();
		}
	}
}