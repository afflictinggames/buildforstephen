#ifndef ANIMATEDPROJECTILE_H_
#define ANIMATEDPROJECTILE_H_

#include "projectile.h"

class AnimatedProjectile : public Projectile
{
public:
	AnimatedProjectile();
	~AnimatedProjectile(void);

	void animate();
};
#endif
